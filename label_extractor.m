function label_extractor(seqName)
% Load all the .csv files in path folder, covert them to MATLAB format,
% convert them to point cloud, extract road lanes labeled points,
% transfrom them from world to ego coordinates
% INPUT: string with name of the road sequence file
% OUTPUT: saved lanesequence.mat file

lidarFiles = dir([seqName, '/lidar/*.csv']);  % dir struct of all pertinent .csv files
semsegFiles = dir([seqName, '/annotations/semseg/*.csv']);
n = length(lidarFiles)-1;  % number of .csv files without poses.csv
m = length(semsegFiles);

if m ~= n
    fprintf('Error: Files do not correspond!\n')
    return
end

worldLines = cell(1,n);

for i = 1:n
    fileName = [seqName, '/lidar/', lidarFiles(i).name];  % specify file path
    lidar = readtable(fileName);  % read each file
    %ptCloud = pointCloud([lidar.x lidar.y lidar.z]);  % load to point cloud

    %fileName = fileName(1:strfind(fileName,'.')-1);  % remove .pkl.csv from file name
    %pcwrite(ptCloud, fileName + ".pcd");  % save lidar point cloud

    lidar = table2array(lidar);  % convert to array
    fileName = [seqName, '/annotations/semseg/', semsegFiles(i).name];  % specify file path
    semseg = readtable(fileName);
    semseg = table2array(semseg);

    labeledPCD = [lidar(:,1:4) semseg(:,2)];  % assign labels to the point cloud

    lanePoints = [];
    for j = 1:length(labeledPCD)  % extract points labeled as "8": "Lane Line Marking" in classes.json
        if labeledPCD(j,5) == 8
            lanePoints = [lanePoints; labeledPCD(j,2:4)];
        end
    end
    
    worldLines{i} = lanePoints;
end

%% transform extracted points into ego coordinates
posesRaw = readtable([seqName, '/lidar/poses.csv'],'ReadVariableNames',false);
posesRaw = table2array(posesRaw);  % messy table with poses in some columns

poses = zeros(length(posesRaw), 7);  % extracted poses [x,y,z,w,x,y,z], where first x,y,z is position and rest is heading
idx = [2,4,6,7,9,11,13];  % columns we are interested in
for i = 1:length(posesRaw)
    for j = 1:7
        poses(i,j) = sscanf(posesRaw{i,idx(j)}, '%f');
    end
end

position = poses(:,1:3);
heading = poses(:,4:7);

% transform points in each frame into ego coordinates
EGOlines = cell(1,n);
for i = 1:length(worldLines)
    points = worldLines{i};
    pos = position(i,:);
    quat = heading(i,:);

    tfMatrix = zeros(4,4);
    tfMatrix(1:3,1:3) = quat2rotm(quat);
    tfMatrix(1:3,4) = pos';
    tfMatrix(4,4) = 1;
    tfMatrix = inv(tfMatrix);

    EGOpoints = (tfMatrix(1:3,1:3)*points' + tfMatrix(1:3,4))';
    EGOlines{i} = EGOpoints;

%     ptCloud = pointCloud(EGOpoints);  % save labeles as pcd
%     fileName = lidarFiles(i).name;
%     fileName = fileName(1:strfind(fileName,'.')-1);
%     pcwrite(ptCloud, fileName + ".pcd");
end

save([seqName, '/laneSequence_', seqName, '.mat'], 'EGOlines')

end