function [finalMean, finalSTD, distError] = get_error(seqName, type)
% Calcualte error of the projected estimated lines
% Works for seq 013 and 043 with labeled camera lines
% INPUT: seqName - name of the sequence
%        type - method to project
% OUTPUT: reprojection error

% Import all data
lineLabeles = importdata([seqName, '\lineLabeles_', seqName, '.mat']);
projectedLines = importdata([seqName, '\projectedLines_', seqName, '_', type, '.mat']); % must be sorted lines to correspond to labeled lines
camera3DLines = importdata([seqName, '\camera3DLines_', seqName, '_', type, '.mat']);

% Import camera parameters
intrinsics = readtable([seqName, '/camera/front_camera/intrinsics.csv'],'ReadVariableNames',false);
intrinsics = table2array(intrinsics);  % camera intrinsics
fx = intrinsics(2,2);
imageWidth = 1920;

% Calculate field of view and pixel angel
FOV = 2*atan(imageWidth / (2*fx));
alpha = FOV / imageWidth;

num_lines = size(lineLabeles, 1);
num_frames = size(lineLabeles, 2);
error = cell(num_lines, num_frames);
distError = [];

for frame = 1:num_frames

    for line = 1:num_lines
        labeledLine = lineLabeles{line, frame};
        estimatedLine = projectedLines{line, frame};

        if isempty(estimatedLine)
            error{line, frame} = NaN;
            continue
        end

        camLine = camera3DLines{line, frame};

        % crop labeles to be the same length as estimates
        M = max(estimatedLine);
        m = min(estimatedLine);
        labeledLine = labeledLine(labeledLine(:,2) <= M(1), :);
        labeledLine = labeledLine(labeledLine(:,1) <= M(2), :);
        labeledLine = labeledLine(labeledLine(:,2) >= m(1), :);
        labeledLine = labeledLine(labeledLine(:,1) >= m(2), :);
        labeledLine = [labeledLine(:,2) labeledLine(:,1)]; % switch

        lenError = NaN(length(labeledLine), 1);

        for pixel = 1:length(labeledLine)
            % find closest estimate for each labeled pixel
            copy = repmat(labeledLine(pixel,:), [length(estimatedLine), 1]);
            distance = sqrt((copy(:,1) - estimatedLine(:,1)).^2 + (copy(:,2) - estimatedLine(:,2)).^2);
            [reproError, idx] = min(distance);

            % find working distance of this estimate
            distance = norm(camLine(idx,:));
            
            % calculate the length error
            arcError = alpha * reproError;
            lenError(pixel) = distance * arcError;
%             distError = [distError; [distance lenError(pixel)]];
        end

        error{line, frame} = lenError;

    end
end

%% Calculate and plot mean and standard deviation
aMean = NaN(num_lines, num_frames);
aSTD = NaN(num_lines, num_frames);

for frame = 1:num_frames
    for line = 1:num_lines
        aMean(line, frame) = mean(error{line, frame}, 'omitnan');
        aSTD(line, frame) = std(error{line, frame}, 'omitnan');
    end
end

finalMean = mean(aMean, 'all', 'omitnan');
finalSTD = std(aSTD, 1, 'all', 'omitnan');

end