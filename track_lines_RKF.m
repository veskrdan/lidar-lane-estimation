function track_lines_RKF(seqName, model)
% Track lane lines of given road sequence using KF algorithm + RUNSAC
% preprocessing
% INPUT: seqname - name of the sequence to be tracked
%        model - system polynomial to be estimated
%              1 - 1st deg. poly
%              2 - 2nd deg. poly
%              3 - 3rd deg. clothoid model
% OUTPUT: saved sequence of estimated line coefficients

% Import RANSAC data
[laneData, estimatedLines] = import_sequence(seqName, ['RANSAC', num2str(model)]);

numLines = size(laneData, 1);
threshold = 0.5;

% Go through all sequence frames
for frame = 1:length(laneData)

    % Go through all lines
    for line = 1:numLines
        oneLine = laneData{line, frame};
        coeffs = estimatedLines{line, frame};
        inliers = [];

        if any(isnan(coeffs))
            continue
        end

        % Find distances of all points from that polynomial
        distance = get_distance(oneLine, coeffs, model);
    
        % Discard outliers, save only points withing the threshold
        for d = 1:length(distance)
            if abs(distance(d)) < threshold
                inliers = [inliers; oneLine(d,:)];
            end
        end
        
        % Probably wrong RANSAC estimate
        if isempty(inliers)
            continue
        end

        laneData{line, frame} = inliers;
    end
end

% Run KF on filtered line points
track_lines_KF(laneData, ['R', num2str(3), seqName])

end