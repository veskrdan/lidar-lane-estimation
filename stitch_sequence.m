function stitch_sequence(seqList, outName)
% Stitch together sorted sequences
% INPUT: seqlist - cell array of seq. file names
%        outname - name of the result

stitchedSequence = cell([]);
for seq = 1:length(seqList)
    EGOpoints = importdata([seqList{seq}, '/laneSequence_', seqList{seq}, '_sorted.mat']);
    stitchedSequence = [stitchedSequence, EGOpoints];
end

save([outName, '.mat'], 'stitchedSequence')

end