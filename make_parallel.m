function [correctedLines] = make_parallel(laneData, estimatedLines)
% Find estimated line with lowest error and make the other parallel to it
% INPUT: laneData - points of lane sequence
%        estimatedLines - estimates to be corrected
% OUTPUT: correctedLines - sequence of corrected line coefficients

numLines = size(laneData, 1);

% Go through all sequence frames
for frame = 1:length(laneData)

    % Find line with the lowest error and take into accout number of points
    lineScore = NaN(numLines,1);
    for line = 1:numLines
        linePoints = laneData{line, frame};
        estimatedLine = estimatedLines{line, frame};

        [lineScore(line)] = line_error(linePoints, estimatedLine);
        lineScore(line) = lineScore(line) + 1/length(linePoints);
    end

    [~, bestLine] = min(lineScore);

    % Parallelize each line to the best estimate
    for line = 1:numLines
        % Skip for the best line
        if line == bestLine
            continue
        end
        
        % Change all coeffs accept the last one for the best estimate
        newCoeffs = estimatedLines{bestLine, frame};
        oldCoeffs = estimatedLines{line, frame};
        newCoeffs(end) = oldCoeffs(end);
        estimatedLines{line, frame} = newCoeffs;
    end
end

correctedLines = estimatedLines;

end