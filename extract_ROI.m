function extract_ROI(seqName)
% Extracts points in ROI in from of the EGO vehicle
% INPUT: string with name of the road sequence file
% OUTPUT: saved lanesequence.mat file with only ROI points
EGOpoints = importdata([seqName, '/laneSequence_', seqName, '.mat']);
ROIpoints = cell(1, length(EGOpoints));

for i = 1:length(EGOpoints)
    ROI = EGOpoints{i};
    ROI = ROI(ROI(:,1) >= -8,:);  % discard values where x < -8
    ROI = ROI(ROI(:,1) <= 8,:);  % discard values where x > 8
    ROI = ROI(ROI(:,2) >= 0,:);  % discard values where y < 0
    ROI = ROI(ROI(:,2) <= 60,:);  % discard values where y < 60
    ROIpoints{i} = [ROI(:,2) ROI(:,1) ROI(:,3)];  % switch x,y so EGO moves in x direction
end

save([seqName, '/laneSequence_', seqName, '.mat'], 'ROIpoints')

end