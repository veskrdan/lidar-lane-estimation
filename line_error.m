function [error] = line_error(linePoints, estimatedLine)
% Compute lateral displacement error as RMSE of the estimated line
% INPUT: linePoints - measured line points
%        estimatedLine - estimated line coefficients
%
% OUTPUT: error - calculated lateral displacement error

if isnan(linePoints)
    error = inf;
    return
end

x = linePoints(:,1);
y = linePoints(:,2);

% Evaluate the estimate at the measurement points
yEst = polyval(estimatedLine,x);

% Find RMSE
xSqr = (yEst - y).^2;
error = sqrt(mean(xSqr));

end