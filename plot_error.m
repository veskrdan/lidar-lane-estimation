function plot_error(seqName)
% Plotting function for final result comparisons

[KF1, stdKF1] = get_error(seqName, 'KF1');
[KF2, stdKF2] = get_error(seqName, 'KF2');
[KF3, stdKF3] = get_error(seqName, 'KF3');
[AKF, stdAKF] = get_error(seqName, 'AKF');

[RANSAC1, stdRANSAC1] = get_error(seqName, 'RANSAC1');
[RANSAC2, stdRANSAC2] = get_error(seqName, 'RANSAC2');
[RANSAC3, stdRANSAC3] = get_error(seqName, 'RANSAC3');
[RKF, stdRKF] = get_error(seqName, 'RKF1');

%% Plot results
x = round([KF1 KF2 KF3 AKF; RANSAC1 RANSAC2 RANSAC3 RKF]*100, 1);
y = [1 2];
e = [stdKF1 stdKF2 stdKF3 stdAKF; stdRANSAC1 stdRANSAC2 stdRANSAC3 stdRKF]*100;

b = barh(y, x, 'EdgeColor', 'w');
hold on

% Find the number of groups and the number of bars in each group
[ngroups, nbars] = size(x);

% Calculate the width for each bar group
groupwidth = min(0.8, nbars/(nbars + 1.5));

% Set the position of each error bar in the centre of the main bar
% Based on barweb.m by Bolu Ajiboye from MATLAB File Exchange
for i = 1:nbars
    % Calculate center of each bar
    c = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    errorbar(x(:,i), c, e(:,i), e(:,i), 'horizontal', 'k', 'linestyle', 'none');
end

% Get group centers
yCnt = get(b(1),'XData') + cell2mat(get(b,'XOffset')); % XOffset is undocumented!
% Create Tick Labels
yLab = {'line KF', 'parabola KF', 'clothoid KF', 'adaptive KF', 'line RANSAC', 'parabola RANSAC', 'clothoid RANSAC', 'parabola RANSAC + KF'}; 
% Set individual ticks
set(gca, 'YTick', sort(yCnt(:)), 'YTickLabel', yLab)

% xticks(0:1:round(max(x) + max(e)))
colors = ["#0072BD", "#D95319", "#EDB120", "#7E2F8E"];

for i=1:length(b)
    xtips1 = b(i).YEndPoints + 0.15;
    ytips1 = b(i).XEndPoints + 0.05;
    labels1 = string(b(i).YData);
    text(xtips1,ytips1,labels1,'VerticalAlignment','middle', 'Color', colors(i))
end

title('Lateral displacement error')
xlabel('average error [cm]')

hold off

end