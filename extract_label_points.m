function extract_label_points(seqName)
% Extract and save the labeld pixels from Image Labeler app
% INPUT: seqName - works only for 013 or 043 sequence now
% OUTPUT: saved labeled lines

labelFiles = dir([seqName, '/PixelLabelData/*.png']);  % dir struct of all labeled .png files
[~,index] = sortrows([labelFiles.datenum].');
labelFiles = labelFiles(index);

num_lines = 4;
classNames = ["line1" "line2" "line3" "line4"];
pixelLabelID = [1 2 3 4];

if strcmp(seqName, '043')
    num_lines = 2;
    classNames = ["line1" "line2"];
    pixelLabelID = [1 2];
end

pixelPoints = cell(num_lines, length(labelFiles));
for frame = 1:length(labelFiles)
    pxDir = fullfile(labelFiles(frame).folder, labelFiles(frame).name);
    pxds = pixelLabelDatastore(pxDir,classNames,pixelLabelID);
    C = read(pxds);
    
    for line = 1:num_lines
        [x, y] = find(C{1} == classNames(line));
        pixelPoints{line, frame} = [x y];
    end
end

save([seqName, '\lineLabeles_', seqName], "pixelPoints")


end