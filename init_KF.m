function [params] = init_KF(options)
    processNoise = options{1};
    measurementNoise = options{2};
    states = options{3};
    covvariance = options{4};

    Q = diag(processNoise.^2); % process noise covariance matrix
    R = diag(measurementNoise.^2); % measurement noise covariance matrix

    x = states'; % initial state estimate
    P = diag(covvariance.^2); % initial error covariance matrix

    params = cell(1,4);
    params{1} = Q;
    params{2} = R;
    params{3} = x;
    params{4} = P;

end