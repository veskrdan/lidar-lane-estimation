function track_lines_AKF(seqName)
% Track lane lines of given road sequence with adaptive Kalman filter
% INPUT: seqname - name of the sequence to be tracked
%        model - system polynomial to be estimated
%              1 - 1st deg. poly
%              2 - 2nd deg. poly
%              3 - 3rd deg. clothoid model
% OUTPUT: saved sequence of estimated line coefficients

[laneData, ~] = import_sequence(seqName, 'sorted');

numLines = size(laneData, 1);
estimatedLines = cell(numLines, length(laneData));
trackedModes = zeros(numLines, length(laneData));
errors = zeros(numLines, 3, length(laneData));
plotModes = 0;

%% Init adaptive Kalman filter

% 1st degree polynomial: y = ax + b
% Define system matrices
F1 = [1 0; 0 1]; % state transition matrix

H1 = @(x)([x 1]); % measurement matrix

% Define noise and states
procesNoise = [1e-2 1e-2];
measurementNoise = 1e1;
initialStates = [0 0];
initialCovariance = [1e2 1e2];

% procesNoise = [1e-6 1e-6];
% measurementNoise = 1e0;
% initialStates = [0 0];
% initialCovariance = [1e3 1e3];

initialOptions = {procesNoise, measurementNoise, initialStates, initialCovariance};

% Get lane matrices (Q,R,x,P)
laneParams1 = cell(numLines,1);
for param = 1:numLines
    [laneParams1{param}] = init_KF(initialOptions);
end

% 2nd degree polynomial: y = ax^2 + bx + c
% Define system matrices
F2 = [1 0 0; 0 1 0; 0 0 1]; % state transition matrix

H2 = @(x)([x^2 x 1]); % measurement matrix

% Define noise and states
procesNoise = [1e-4 1e-4 1e-4];
measurementNoise = 1e1;
initialStates = [0 0 0];
initialCovariance = [1e3 1e3 1e3];

initialOptions = {procesNoise, measurementNoise, initialStates, initialCovariance};

% Get lane matrices (Q,R,x,P)
laneParams2 = cell(numLines,1);
for param = 1:numLines
    [laneParams2{param}] = init_KF(initialOptions);
end

% Clothoid model: y = y0 + 1/2*cO*x^2 + 1/6*c1*x^3
% Define system matrices
F3 = [1 0 0; 0 1 0; 0 0 1]; % state transition matrix

H3 = @(x)([1 1/2*x^2 1/6*x^3]); % measurement matrix

% Define noise and states
procesNoise = [1e-4 1e-4 1e-4];
measurementNoise = 1e1;
initialStates = [0 0 0];
initialCovariance = [1e5 1e5 1e5];

initialOptions = {procesNoise, measurementNoise, initialStates, initialCovariance};

% Get lane matrices (Q,R,x,P)
laneParams3 = cell(numLines,1);
for param = 1:numLines
    [laneParams3{param}] = init_KF(initialOptions);
end


%% Run AKF
% Go through all sequence frames
disp('Tracking started...')
for frame = 1:length(laneData)

    % Go through all lines
    for line = 1:numLines

        % Sort line by x
        oneLine = laneData{line, frame};
        oneLine = sortrows(oneLine, 1);

        % Go through all points of each line
        for point = 1:size(oneLine,1)

            % Prediction
            [laneParams1{line}{3}, laneParams1{line}{4}] = predict_KF(laneParams1{line},F1);
            [laneParams2{line}{3}, laneParams2{line}{4}] = predict_KF(laneParams2{line},F2);
            [laneParams3{line}{3}, laneParams3{line}{4}] = predict_KF(laneParams3{line},F3);

            % Get measurement
            if isnan(oneLine)
                continue
            else
                z = oneLine(point, 1:2); % [x,y]
            end

            % Measurement update
            [laneParams1{line}{3}, laneParams1{line}{4}] = correct_KF(laneParams1{line},H1(z(1)),z(2));
            [laneParams2{line}{3}, laneParams2{line}{4}] = correct_KF(laneParams2{line},H2(z(1)),z(2));
            [laneParams3{line}{3}, laneParams3{line}{4}] = correct_KF(laneParams3{line},H3(z(1)),z(2));
        end

        % Evaluate tracked polynomial coefficients
        coeffs1 = laneParams1{line}{3};
        coeffs2 = laneParams2{line}{3};
        coeffs3 = laneParams3{line}{3};
        coeffs3 = [1/6*coeffs3(3) 1/2*coeffs3(2) 0 coeffs3(1)];
        coeffs = {coeffs1 coeffs2 coeffs3};

        % Choose the best fit
        error1 = line_error(oneLine, coeffs1);
        error2 = line_error(oneLine, coeffs2);
        error3 = line_error(oneLine, coeffs3);

        errors(line, 1, frame) = error1;
        errors(line, 2, frame) = error2;
        errors(line, 3, frame) = error3;

        [~,I] = min([error1 error2 error3]);
        
        % Save estimated coefficients
        estimatedLines{line, frame} = coeffs{I};
        trackedModes(line, frame) = I;
    end
end

%% Make the lines parallel to the best estimate
[estimatedLines, modes] = make_parallel_AKF(laneData, estimatedLines, trackedModes);
disp('Tracking finished!')

%% Plot mode changes
if plotModes
    yyaxis left
    stairs(1:1:length(laneData), modes, 'LineWidth', 1.5)
    yticks([1 2 3])
    yticklabels({'line', 'parabola', 'clothoid'})
    xlabel('frame number')
    
    % average error of one model per all lines in a frame
    error = zeros(3,length(laneData));
    for f = 1:length(laneData)
        error(1,f) = mean(errors(:,1,f));
        error(2,f) = mean(errors(:,2,f));
        error(3,f) = mean(errors(:,3,f));
    end

    yyaxis right
    plot(1:1:length(laneData), error(1,:), 'LineWidth', 1)
    hold on
    plot(1:1:length(laneData), error(2,:), 'LineWidth', 1, 'Color', [0.9290 0.6940 0.1250], 'LineStyle', '-')
    plot(1:1:length(laneData), error(3,:), 'LineWidth', 1, 'Color', [0.4940 0.1840 0.5560], 'LineStyle', '-')
    legend('modes','line er.','parabola er.','clothoid er.')

end

%% Save the estimated coefficients
try
    disp('Saving data to folder...')
    save([seqName, '/lanesequence_', seqName, '_AKF.mat'], 'estimatedLines')
    disp('Save successful!')
catch
    disp('No such sequence folder to save into. Saving in current working directory...')
    save([seqName, '_AKF.mat'], 'estimatedLines')
    disp('Save successful!')
end

end