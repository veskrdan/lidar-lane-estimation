function [predicted_x, predicted_P] = predict_KF(params,F)
    % KF prediction step
    Q = params{1};
    x = params{3};
    P = params{4};

    predicted_x = F*x; % state prediction
    predicted_P = F*P*F' + Q; % error covariance prediction

end