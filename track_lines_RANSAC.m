function track_lines_RANSAC(seqName, model)
% Track lane lines of given road sequence using RANSAC algorithm
% INPUT: seqname - name of the sequence to be tracked
%        model - system polynomial to be estimated
%              1 - 1st deg. poly
%              2 - 2nd deg. poly
%              3 - 3rd deg. clothoid model
% OUTPUT: saved sequence of estimated line coefficients

[laneData, ~] = import_sequence(seqName, 'sorted');

numLines = size(laneData, 1);
estimatedLines = cell(numLines, length(laneData));

%% Run RANSAC

% Init
outliers = 5/10;
threshold = 1/10;

% Go through all sequence frames
disp('Tracking started...')
for frame = 1:length(laneData)

    % Go through all lines
    for line = 1:numLines
        oneLine = laneData{line, frame};

        if size(oneLine,1) < model+1  % not enought poitns
            coeffs = NaN;
        else
            [coeffs] = run_RANSAC(oneLine, outliers, threshold, model);
        end
        
        % Save estimated coefficients
        estimatedLines{line, frame} = coeffs;
    end
end

%% Make the lines parallel to the best estimate
[estimatedLines] = make_parallel(laneData, estimatedLines);
disp('Tracking finished!')

%% Find optimal RANSAC settings
% Get error of each line from all frames
error = NaN(1, length(laneData));
for frame = 1:length(laneData)
    for line = 1:size(estimatedLines,1)
        linePoints = laneData{line, frame};
        estimatedLine = estimatedLines{line, frame};
        [error(line, frame)] = line_error(linePoints, estimatedLine);

        if error(line, frame) == inf
            error(line, frame) = NaN;
        end
    end
end

%% Save the estimated coefficients
try
    disp('Saving data to folder...')
    save([seqName, '/lanesequence_', seqName, '_RANSAC', num2str(model), '.mat'], 'estimatedLines')
    disp('Save successful!')
catch
    disp('No such sequence folder to save into. Saving in current working directory...')
    save([seqName, '_RANSAC', num2str(model), '.mat'], 'estimatedLines')
    disp('Save successful!')
end

end