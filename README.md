# Lane Estimation thesis project
Please, reffer to the Matlab live script tutorial, where you can find how to run the estimation process. For more details, visit the master thesis itself in the PDF file.

This repository includes data from the publicly available PandaSet, which is subjected to their licence terms (and any generated data). This data was used for academic purposes.

The code is open-source. Feel free to use any of the data (except the above mentioned) for personal or academic purposes, citing them properly as it should be.