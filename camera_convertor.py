import pandas as pd
import os
import sys

n = str(sys.argv[1])

# load camera poses.json and instrinsics.json and convert it to .csv and save
dir = os.path.join(n, 'camera/front_camera/poses.json')
df = pd.read_json(dir)
extract_path = dir.replace(".json", "")
df.to_csv(extract_path+'.csv')

# must add [] around the intrinsics in .json
dir = os.path.join(n, 'camera/front_camera/intrinsics.json')
df = pd.read_json(dir)
extract_path = dir.replace(".json", "")
df.to_csv(extract_path+'.csv')