function sort_lines_simple(seqName, x)
% Sort points into lines by y value, expect 2 lines
% INPUT: string with name of the road sequence file and y value
% OUTPUT: saved sorted lanesequence.mat file

rawPoints = importdata([seqName, '/laneSequence_', seqName, '.mat']);
sortedPoints = cell(2, length(rawPoints));

for i = 1:length(rawPoints)
    points = rawPoints{i};
    left = points(points(:,2) > x,:); % points > x
    right = points(points(:,2) < x,:); % points < x

    sortedPoints{1,i} = left;
    sortedPoints{2,i} = right;
end

save([seqName, '/laneSequence_', seqName, '_sorted.mat'], 'sortedPoints')

end