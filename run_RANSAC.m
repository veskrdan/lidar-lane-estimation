function [polynomial] = run_RANSAC(data, outliers, threshold, degree)
% Perform RAndom SAmple Consensus algorithm
% INPUT: 
%       data: n-by-2 vector with x,y points
%       outliers: percentage of outliers
%       threshold: maximum distance of an inlier from the polynomial
%       degree: degree of polynomial to be fitted
%               1: 1st deg poly, 2: 2nd deg poly, 3: 3rd deg clothoid
%
% OUTPUT:
%       polynomial: fitted polynomial

if degree < 1 || degree > 3
    error("Wrong degree number!")
end

% Number of RANSAC iterations
p = 0.99; % probability of succes
eps = outliers;
s = degree + 1; % minimum number of points for n degree pol. is n+1

if degree == 3 % clothoid has x^1 = 0
    s = degree;
end

num_iter = log(1-p)/log(1-(1-eps)^s) + sqrt(1-(1-eps)^s)/(1-eps)^s;
num_iter = round(num_iter);

if degree == 3
    polynomial = zeros(s + 2, num_iter); % [inliers count + deg num of coeffs]
else
    polynomial = zeros(s + 1, num_iter); % [inliers count + deg num of coeffs]
end

for i = 1:num_iter
    % Pick random s points
    rp = randperm(size(data,1), s);
    switch degree
        case 1
            pts = [data(rp(1),1) data(rp(1),2);
                   data(rp(2),1) data(rp(2),2)];

            % Fit them with a polynomial
            [coeffs] = fit_poly_nhom(pts(:,1), pts(:,2), degree);

            % Save the polynomial
            polynomial(:,i) = [0 coeffs'];

        case 2
            pts = [data(rp(1),1) data(rp(1),2);
                   data(rp(2),1) data(rp(2),2);
                   data(rp(3),1) data(rp(3),2)];

            % Fit them with a polynomial
            [coeffs] = fit_poly_nhom(pts(:,1), pts(:,2), degree);

            % Save the polynomial
            polynomial(:,i) = [0 coeffs'];

        case 3
            pts = [data(rp(1),1) data(rp(1),2);
                   data(rp(2),1) data(rp(2),2);
                   data(rp(3),1) data(rp(3),2)];

            % Fit them with a polynomial
            [coeffs] = fit_poly_nhom(pts(:,1), pts(:,2), degree);

            % Save the polynomial
            polynomial(:,i) = [0 coeffs(1) coeffs(2) 0 coeffs(3)];
    end

    % Find distances of all points from that polynomial
    d = get_distance(data, polynomial(2:end, i), degree);

    % Find the number of inliers
    for j = 1:length(d)
        if abs(d(j)) < threshold
            polynomial(1,i) = polynomial(1,i) + 1;
        end
    end

end

% Find the polynomial with most inliers
[~,I] = max(polynomial(1,:));

polynomial = polynomial(2:end, I);

end

function [coeffs] = fit_poly_nhom(x, y, degree)
% INPUT: 
%       x: query points as a column vector
%       y: fitted values at x as a column vector
%       degree: deg of the polynomial
%
% OUTPUT:
%       coeffs: coefficients of the polynomial

switch degree
    case 1
        V = [x, ones(length(x),1)];  % Vandermonde matrix
    case 2
        V = [x.^2, x, ones(length(x),1)];  % Vandermonde matrix
    case 3
        V = [1/6*x.^3, 1/2*x.^2, ones(length(x),1)];  % Vandermonde matrix
end

coeffs = V\y;  % Solve system of equations

% V matrix is singular when there are same points
% As a result the 'a' index is NaN
if isnan(coeffs(1))
    coeffs = zeros(1, degree + 1);
end

end