function [EGOpoints, EGOlines] = import_sequence(seqName, type)
% Import data from specified sequence and catch edge cases
% INPUT: seqname - sequence file number/name
%        type - data type: sorted/unsorted or algorithm used

try
    disp('Importing data from folder...')
    if strcmp(type, 'sorted')
        EGOpoints = importdata([seqName, '/laneSequence_', seqName, '_sorted.mat']);
    elseif strcmp(type, 'unsorted')
        EGOpoints = importdata([seqName, '/laneSequence_', seqName, '.mat']);
    else
        EGOlines = importdata([seqName, '/laneSequence_', seqName, '_', type, '.mat']);
        EGOpoints = importdata([seqName, '/laneSequence_', seqName, '_sorted.mat']);
    end
    disp('Import successful!')
catch
    disp('No such sequence folder here. Searching files...')
    if strcmp(type, 'sorted')
        EGOpoints = importdata([seqName, '.mat']);
    else
        EGOlines = importdata([seqName, '_', type, '.mat']);
        EGOpoints = importdata([seqName, '.mat']);
    end
    disp('Import successful!')
end

if strcmp(type, 'sorted') || strcmp(type, 'unsorted')
    EGOlines = NaN;
end

end