function [planeParams] = fit_plane(seqName)
% Fit plane through sequence of point cloud and save

[ROIpoints, ~] = import_sequence(seqName, 'unsorted');

planeParams = cell(length(ROIpoints), 1);

for frame = 1:length(ROIpoints)
    points = ROIpoints{frame};
    ptCloud = pointCloud(points);  % load to point cloud
    
    maxDistance = 0.1;
    referenceVector = [0 0 1];
    model = pcfitplane(ptCloud,maxDistance,referenceVector);
    planeParams{frame} = model.Parameters;
end

end