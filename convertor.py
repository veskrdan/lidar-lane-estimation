import pickle as pkl
import pandas as pd
import os
import gzip, shutil
import sys

n = str(sys.argv[1])
# Loop through all files in the directory
# os.listdir(dir) returns a list of files and directories inside dir.
dir = os.path.join(n, 'annotations/semseg/')
for file in os.listdir(dir):
    # Filter the GZIP file using the file extension
    if file.endswith(".gz"):
        # Join the dir folder with the file name to get a valid file path of GZIP.
        gz_path = os.path.join(dir, file)
        
        # Join the dir with the file name to get a valid file path of the extracted file.
        extract_path = os.path.join(dir, file.replace(".gz", ""))

        # Open GZIP with gzip and write the content into outfile using shutil.
        with gzip.open(gz_path, "rb") as infile, open(
            extract_path, "wb"
        ) as outfile:
            shutil.copyfileobj(infile, outfile)
        
        # Open the extracted pkl file and convert it to csv and save
        with open(extract_path, "rb") as f:
            object = pkl.load(f)

        df = pd.DataFrame(object)
        df.to_csv(extract_path+'.csv')
        # Uncomment this line if you want to remove the GZIP and PKL file.
        #os.remove(gz_path)
        #os.remove(extract_path)

dir = os.path.join(n, 'lidar/')
for file in os.listdir(dir):
    # Filter the GZIP file using the file extension
    if file.endswith(".gz"):
        # Join the dir folder with the file name to get a valid file path of GZIP.
        gz_path = os.path.join(dir, file)
        
        # Join the dir with the file name to get a valid file path of the extracted file.
        extract_path = os.path.join(dir, file.replace(".gz", ""))

        # Open GZIP with gzip and write the content into outfile using shutil.
        with gzip.open(gz_path, "rb") as infile, open(
            extract_path, "wb"
        ) as outfile:
            shutil.copyfileobj(infile, outfile)
        
        # Open the extracted pkl file and convert it to csv and save
        with open(extract_path, "rb") as f:
            object = pkl.load(f)

        df = pd.DataFrame(object)
        df.to_csv(extract_path+'.csv')
        # Uncomment this line if you want to remove the GZIP and PKL file.
        #os.remove(gz_path)
        #os.remove(extract_path)

# load lidar poses.json and convert it to .csv and save
dir = os.path.join(n, 'lidar/poses.json')
df = pd.read_json(dir)
extract_path = dir.replace(".json", "")
df.to_csv(extract_path+'.csv')

print("Sequence converted!")
