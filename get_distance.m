function [d] = get_distance(X, coeffs, degree)
% INPUT: 
%       X: n-by-2 vector with data
%       coeffs: polynomial coefficients
%
% OUTPUT:
%       d: 1-by-N vector of *signed* distances of all points 
%          from the polynomial. 
 
d = zeros(1,size(X,1));

switch degree
    case 1
        a = coeffs(1);
        b = coeffs(2);

        % Evaluate for each point
        for i = 1:size(X,1)
            x = X(i,1);
            y = X(i,2);
        
            d(i) = abs(a*x - y + b)/sqrt(a^2 + 1);
        end

    case 2
        a = coeffs(1);
        b = coeffs(2);
        c = coeffs(3);

        % Distance from the polynomial:
        % 2nd degree pol. f(x) = ax^2 + bx + c
        % its derivative: f'(x) = 2ax + b
        % distance eq.:   y0-f(x)/x0-x = -1/f'(x)
        % gives 3rd pol.: p = [2*a^2  3*a*b   2*a*c-2*a*y0+b^2+1 b*c-b*y0-x0];
        
        % Evaluate for each point
        for i = 1:size(X,1)
            p = [2*a^2  3*a*b   2*a*c-2*a*X(i,2)+b^2+1  b*c-b*X(i,2)-X(i,1)];
            r = roots(p);
            x = min(r(imag(r)==0)); % Sort out real roots
            y = polyval([a b c],x);
        
            d(i) = sqrt((x - X(i,1))^2 + (y - X(i,2))^2); % Norm = distance
        end

    case 3
        a = coeffs(1);
        b = coeffs(2);
        c = coeffs(3);
        e = coeffs(4);

        % Distance from the polynomial:
        % 3rd degree pol. f(x) = ax^3 + bx^2 + cx + d; c = 0
        % its derivative: f'(x) = 3ax^2 + 2bx + d
        % distance eq.:   y0-f(x)/x0-x = -1/f'(x)
        % gives 5th pol.: p = [3*a^2  5*a*b a*d+2*b^2 3*a*d-3*a*y0+b*d 2*b*d-2*b*y0 d^2-d*y0];
        
        % Evaluate for each point
        for i = 1:size(X,1)
            x0 = X(i,1);
            y0 = X(i,2);
            p = [3*a^2  5*a*b a*e+2*b^2 3*a*e-3*a*y0+b*e 2*b*e-2*b*y0+1 e^2-e*y0-x0];
            r = roots(p);
            x = min(r(imag(r) == 0)); % Sort out real roots
            y = polyval([a b c e],x);
        
            d(i) = sqrt((x - X(i,1))^2 + (y - X(i,2))^2); % Norm = distance
        end
end

end