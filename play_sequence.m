function play_sequence(seqName, type)
% Play points from a sequence
% INPUT: seqname - sequence file number/name
%        type - data type: sorted/unsorted or estimated

% Find correct files to import
[EGOpoints, EGOlines] = import_sequence(seqName, type);

f = figure;
for frame = 1:length(EGOpoints)
    for line = 1:size(EGOpoints,1)
        txt = ['line points ',num2str(line)];
        if ~isnan(EGOpoints{line,frame})
            scatter(EGOpoints{line,frame}(:,2),EGOpoints{line,frame}(:,1), 10,'filled','square','DisplayName',txt);
        end
        [t, s] = title('EGO view', ['Sequence: ', seqName, '   Mode: ', type]);
        t.FontSize = 13;
        s.FontAngle = 'italic';
        legend
        hold on
        xlabel('x [m]')
        ylabel('y [m]')
        xlim([-8,8])
        ylim([0,65])

        if ~strcmp(type, 'sorted') && ~strcmp(type, 'unsorted')
            txt = ['estimated line ',num2str(line)];
            coeffs = EGOlines{line,frame};
            x = 0:0.1:70;
            y = polyval(coeffs, x');
            plot(y,x,'LineWidth', 1.5, 'DisplayName', txt);
        end
    end

    pause(0.2)

    if ~ishghandle(f)
        break
    end

    hold off
end

end