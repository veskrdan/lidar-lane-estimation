function [corrected_x, corrected_P] = correct_KF(params,H,z)
    % KF measurement update step
    R = params{2};
    x = params{3};
    P = params{4};

    K = P*H'/(H*P*H' + R); % Kalman gain
    corrected_x = x + K*(z - H*x); % state correction
    corrected_P = (eye(length(x)) - K*H)*P; % error covariance correction

%     K = P*H'/(H*P*H' + R); % Kalman gain
%     corrected_x = x + K*(z - h); % state correction
%     corrected_P = (eye(length(x)) - K*H)*P; % error covariance correction

end