function [correctedLines, modes] = make_parallel_AKF(laneData, estimatedLines, trackedModes)
% Find estimated line with lowest error and make the other parallel to it
% INPUT: laneData - points of lane sequence
%        estimatedLines - estimates to be corrected
%        trackedModes - models chosen by AKF
% OUTPUT: correctedLines - sequence of corrected line coefficients

numLines = size(laneData, 1);
modes = zeros(1, length(laneData));

% Go through all sequence frames
for frame = 1:length(laneData)

    % Find line with the lowest error and take into accout number of points
    % and most times chosen mode
    lineScore = NaN(numLines,1);
    theMode = mode(trackedModes(:,frame));
    for line = 1:numLines
        linePoints = laneData{line, frame};
        estimatedLine = estimatedLines{line, frame};

        [lineScore(line)] = line_error(linePoints, estimatedLine);
        lineScore(line) = lineScore(line) + 1/length(linePoints);  % penalize poorer detection

        if trackedModes(line, frame) ~= theMode
            lineScore(line) = lineScore(line)*1.35;  % penalize sparserly chosen mode (1x for 013)
        end
    end

    [~, bestLine] = min(lineScore);
    modes(frame) = trackedModes(bestLine, frame);

    % Parallelize each line to the best estimate
    for line = 1:numLines
        % Skip for the best line
        if line == bestLine
            continue
        end
        
        % Change all coeffs accept the last one for the best estimate
        newCoeffs = estimatedLines{bestLine, frame};
        oldCoeffs = estimatedLines{line, frame};
        newCoeffs(end) = oldCoeffs(end);
        estimatedLines{line, frame} = newCoeffs;
    end
end

correctedLines = estimatedLines;

end