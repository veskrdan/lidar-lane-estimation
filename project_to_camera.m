function project_to_camera(seqName, type, doPlot)
% Project tracking results onto front camera images
% INPUT: seqName - name of the sequence
%        type - method to project
%        doPlot - display the projection (0 - no, 1 - yes)
% OUTPUT: saved projected estimates

%% Import EGO points and estimated lines
[EGOpoints, EGOlines] = import_sequence(seqName, type);

%% Import lidar poses
posesRaw = readtable([seqName, '/lidar/poses.csv'],'ReadVariableNames',false);
posesRaw = table2array(posesRaw);  % messy table with poses in some columns

lidarPoses = zeros(length(posesRaw), 7);  % extracted poses [x,y,z,w,x,y,z], where first x,y,z is position and rest is heading
idx = [2,4,6,7,9,11,13];  % columns we are interested in
for i = 1:length(posesRaw)
    for j = 1:7
        lidarPoses(i,j) = sscanf(posesRaw{i,idx(j)}, '%f');
    end
end

%% Import camera poses
posesRaw = readtable([seqName, '/camera/front_camera/poses.csv'],'ReadVariableNames',false);
posesRaw = table2array(posesRaw);  % messy table with poses in some columns

cameraPoses = zeros(length(posesRaw), 7);  % extracted poses [x,y,z,w,x,y,z], where first x,y,z is position and rest is heading
idx = [2,4,6,7,9,11,13];  % columns we are interested in
for i = 1:length(posesRaw)
    for j = 1:7
        cameraPoses(i,j) = sscanf(posesRaw{i,idx(j)}, '%f');
    end
end

%% Import camera intrinsics
intrinsics = readtable([seqName, '/camera/front_camera/intrinsics.csv'],'ReadVariableNames',false);
intrinsics = table2array(intrinsics);  % camera intrinsics

K = eye(3);
K(1,1) = intrinsics(2,2); % fx
K(2,2) = intrinsics(2,3); % fy
K(1,3) = intrinsics(2,4); % cx
K(2,3) = intrinsics(2,5); % cy

%% Import camera images
imageFiles = dir([seqName, '/camera/front_camera/*.jpg']);

%% Import plane parameters
planeParams = fit_plane(seqName);

%% Run projection
numLines = size(EGOlines, 1);
projectedLines = cell(numLines, length(EGOpoints));
camera3DLines = cell(numLines, length(EGOpoints));

for frame = 1:length(EGOpoints)
    linePoints = cell(numLines,1);
    lidarPoints = cell(numLines,1);
    plane = planeParams{frame};

    % load point cloud and estimates
    for line = 1:numLines
        coeffs = EGOlines{line,frame};
        points = EGOpoints{line, frame};
        lidarPoints{line} = [points(:,2) points(:,1) points(:,3)];
        
        % estimate z-coord from plane model
        x = 0:0.01:70;
        y = polyval(coeffs, x');
        z = (-plane(1)*x' - plane(2)*y - plane(4))/plane(3);
        
        points = [y x' z];
        linePoints{line} = points;
    end

    % Transform estimated and LIDAR points into world coordinates
    position = lidarPoses(:,1:3);
    heading = lidarPoses(:,4:7);
    
    pos = position(frame,:);
    quat = heading(frame,:);
    
    tfMatrix = zeros(4,4);
    tfMatrix(1:3,1:3) = quat2rotm(quat);
    tfMatrix(1:3,4) = pos';
    tfMatrix(4,4) = 1;
    tfMatrix = inv(tfMatrix);
    
    worldLines = cell(numLines, 1);
    for line = 1:numLines
        % estimated lines
        worldPoints = tfMatrix(1:3,1:3)'*(linePoints{line}' - tfMatrix(1:3,4));
        worldLines{line} = worldPoints';
        
        % LIDAR points
        worldPoints = tfMatrix(1:3,1:3)'*(lidarPoints{line}' - tfMatrix(1:3,4));
        lidarPoints{line} = worldPoints';
    end

    % Project from world to camera coordinates
    position = cameraPoses(:,1:3);
    heading = cameraPoses(:,4:7);
    
    pos = position(frame,:);
    quat = heading(frame,:);
    
    tfMatrix = zeros(4,4);
    tfMatrix(1:3,1:3) = quat2rotm(quat);
    tfMatrix(1:3,4) = pos';
    tfMatrix(4,4) = 1;
    tfMatrix = inv(tfMatrix);

    cameraLines = cell(numLines, 1);
    cameraPoints3D = cell(numLines, 1);
    for line = 1:numLines
        % estimated lines
        cameraPoints = (tfMatrix(1:3,1:3)*worldLines{line}' + tfMatrix(1:3,4))';
        cameraPoints = cameraPoints(cameraPoints(:,3) > 0, :);  % filter out z < 0
        cameraPoints3D{line} = cameraPoints;
        
        cameraPoints2D = K*cameraPoints';
        cameraLines{line} = (cameraPoints2D(1:2,:) ./ cameraPoints2D(3,:))';

        % LIDAR points
        cameraPoints = (tfMatrix(1:3,1:3)*lidarPoints{line}' + tfMatrix(1:3,4))';
        cameraPoints = cameraPoints(cameraPoints(:,3) > 0, :);  % filter out z < 0
        
        cameraPoints2D = K*cameraPoints';
        lidarPoints{line} = (cameraPoints2D(1:2,:) ./ cameraPoints2D(3,:))';
    end

    % Project onto the image
    fileName = [seqName, '/camera/front_camera/', imageFiles(frame).name];
    image = imread(fileName);
    [height, width, ~] = size(image);
    
    if doPlot
        imshow(fileName)
        hold on
    end
    
    % delete outliers
    for line = 1:numLines
        % LIDAR points
        cameraPoints2D = lidarPoints{line};
        cameraPoints2D = cameraPoints2D(cameraPoints2D(:,1) > 0, :);
        cameraPoints2D = cameraPoints2D(cameraPoints2D(:,2) > 0, :);
        cameraPoints2D = cameraPoints2D(cameraPoints2D(:,1) < width, :);
        cameraPoints2D = cameraPoints2D(cameraPoints2D(:,2) < height, :);
    
        if doPlot
            sz = linspace(5, 1, length(cameraPoints2D));
            scatter(cameraPoints2D(:,1), cameraPoints2D(:,2), sz, 'filled', 'y')
        end

        % estimated lines
        cameraPoints2D = cameraLines{line};
        temp = [cameraLines{line} cameraPoints3D{line}];
        cameraPoints2D = cameraPoints2D(cameraPoints2D(:,1) > 0, :);
        cameraPoints2D = cameraPoints2D(cameraPoints2D(:,2) > 0, :);
        cameraPoints2D = cameraPoints2D(cameraPoints2D(:,1) < width, :);
        cameraPoints2D = cameraPoints2D(cameraPoints2D(:,2) < height, :);
        projectedLines{line, frame} = cameraPoints2D;
        
        % camera frame lines for distance
        temp = temp(temp(:,1) > 0, :);
        temp = temp(temp(:,2) > 0, :);
        temp = temp(temp(:,1) < width, :);
        temp = temp(temp(:,2) < height, :);
        camera3DLines{line, frame} = temp(:,3:end);
    
        if doPlot
            sz = linspace(5, 1, length(cameraPoints2D));
            scatter(cameraPoints2D(:,1), cameraPoints2D(:,2), sz, 'filled')
        end
    end
    
    if doPlot
        pause(0.3)
        hold off
    end
end

% sort projected lines to correspond to labeled lines
if strcmp(seqName, '013') || strcmp(seqName, '043')
    for frame = 1:length(EGOpoints)
        if strcmp(seqName, '013')
            line1 = projectedLines{1, frame};
            line2 = projectedLines{2, frame};
            line3 = projectedLines{3, frame};
        
            projectedLines{1, frame} = line2;
            projectedLines{2, frame} = line3;
            projectedLines{3, frame} = line1;
    
            line1 = camera3DLines{1, frame};
            line2 = camera3DLines{2, frame};
            line3 = camera3DLines{3, frame};
        
            camera3DLines{1, frame} = line2;
            camera3DLines{2, frame} = line3;
            camera3DLines{3, frame} = line1;
        elseif strcmp(seqName, '043')
            line1 = projectedLines{1, frame};
            line2 = projectedLines{2, frame};
        
            projectedLines{1, frame} = line2;
            projectedLines{2, frame} = line1;
    
            line1 = camera3DLines{1, frame};
            line2 = camera3DLines{2, frame};
        
            camera3DLines{1, frame} = line2;
            camera3DLines{2, frame} = line1;
        end
    end
end

save([seqName, '/projectedLines_', seqName, '_', type, '.mat'], "projectedLines")
save([seqName, '/camera3DLines_', seqName, '_', type, '.mat'], "camera3DLines")

end