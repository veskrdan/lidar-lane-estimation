function track_lines_KF(seqName, model)
% Estimate lane lines of a given road sequence
% INPUT: seqname - name of the sequence to be tracked
%        model - system polynomial to be estimated
%              1 - 1st deg. poly
%              2 - 2nd deg. poly
%              3 - 3rd deg. clothoid model
% OUTPUT: saved sequence of estimated line coefficients

% Special case for RANSAC + KF
if strcmp(model(1), 'R')
    laneData = seqName;
    seqName = model(3:end);
    model = str2num(model(2));
    type = 'R';
else
    [laneData, ~] = import_sequence(seqName, 'sorted');
    type = NaN;
end

numLines = size(laneData, 1);
estimatedLines = cell(numLines, length(laneData));

%% Init Kalman filter
switch model
    case 1
        % 1st degree polynomial: y = ax + b
        % Define system matrices
        F = [1 0; 0 1]; % state transition matrix
        
        H = @(x)([x 1]); % measurement matrix
        
        % Define noise and states
        procesNoise = [1e-2 1e-2];
        measurementNoise = 1e1;
        initialStates = [0 0];
        initialCovariance = [1e2 1e2];

        % Different combinations can be tried
%         procesNoise = [1e-6 1e-6];
%         measurementNoise = 1e0;
%         initialStates = [0 0];
%         initialCovariance = [1e3 1e3];

        initialOptions = {procesNoise, measurementNoise, initialStates, initialCovariance};
        
        % Get lane matrices (Q,R,x,P)
        laneParams = cell(numLines,1);
        for param = 1:numLines
            [laneParams{param}] = init_KF(initialOptions);
        end
    case 2
        % 2nd degree polynomial: y = ax^2 + bx + c
        % Define system matrices
        F = [1 0 0; 0 1 0; 0 0 1]; % state transition matrix
        
        H = @(x)([x^2 x 1]); % measurement matrix
        
        % Define noise and states
        procesNoise = [1e-4 1e-4 1e-4];
        measurementNoise = 1e1;
        initialStates = [0 0 0];
        initialCovariance = [1e3 1e3 1e3];

        initialOptions = {procesNoise, measurementNoise, initialStates, initialCovariance};
        
        % Get lane matrices (Q,R,x,P)
        laneParams = cell(numLines,1);
        for param = 1:numLines
            [laneParams{param}] = init_KF(initialOptions);
        end
    case 3
        % Clothoid model: y = y0 + 1/2*cO*x^2 + 1/6*c1*x^3
        % Define system matrices
        F = [1 0 0; 0 1 0; 0 0 1]; % state transition matrix
        
        H = @(x)([1 1/2*x^2 1/6*x^3]); % measurement matrix
        
        % Define noise and states
        procesNoise = [1e-4 1e-4 1e-4];
        measurementNoise = 1e1;
        initialStates = [0 0 0];
        initialCovariance = [1e5 1e5 1e5];

        initialOptions = {procesNoise, measurementNoise, initialStates, initialCovariance};
        
        % Get lane matrices (Q,R,x,P)
        laneParams = cell(numLines,1);
        for param = 1:numLines
            [laneParams{param}] = init_KF(initialOptions);
        end
end

%% Run KF
% Go through all sequence frames
disp('Tracking started...')
for frame = 1:length(laneData)

    % Go through all lines
    for line = 1:numLines

        % Sort line by x
        oneLine = laneData{line, frame};
        oneLine = sortrows(oneLine, 1);

        % Go through all points of each line
        for point = 1:size(oneLine,1)

            % Prediction
            [laneParams{line}{3}, laneParams{line}{4}] = predict_KF(laneParams{line},F);

            % Get measurement
            if isnan(oneLine)
                continue
            else
                z = oneLine(point, 1:2); % [x,y]
            end

            % Measurement update
            [laneParams{line}{3}, laneParams{line}{4}] = correct_KF(laneParams{line},H(z(1)),z(2));
        end

        % Evaluate tracked polynomial coefficients
        if model ~= 3
            coeffs = laneParams{line}{3};
        else
            coeffs = laneParams{line}{3};
            coeffs = [1/6*coeffs(3) 1/2*coeffs(2) 0 coeffs(1)];
        end
        
        % Save estimated coefficients
        estimatedLines{line, frame} = coeffs;
    end
end

%% Make the lines parallel to the best estimate
[estimatedLines] = make_parallel(laneData, estimatedLines);
disp('Tracking finished!')

%% Save the estimated coefficients
if strcmp(type, 'R')
    try
        disp('Saving data to folder...')
        save([seqName, '/lanesequence_', seqName, '_RKF', num2str(model), '.mat'], 'estimatedLines')
        disp('Save successful!')
    catch
        disp('No such sequence folder to save into. Saving in current working directory...')
        save([seqName, '_RKF', num2str(model), '.mat'], 'estimatedLines')
        disp('Save successful!')
    end
else
    try
        disp('Saving data to folder...')
        save([seqName, '/lanesequence_', seqName, '_KF', num2str(model), '.mat'], 'estimatedLines')
        disp('Save successful!')
    catch
        disp('No such sequence folder to save into. Saving in current working directory...')
        save([seqName, '_KF', num2str(model), '.mat'], 'estimatedLines')
        disp('Save successful!')
    end
end

end