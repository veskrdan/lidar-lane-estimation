function sort_lines(seqName, n)
% Sort points into lines using sliding window technique
% INPUT: string with name of the road sequence file and number of lines
% OUTPUT: saved sorted lanesequence.mat file
plotLines = 0;
plotWindows = 0;

rawPoints = importdata([seqName, '/lanesequence_', seqName, '.mat']);
sortedPoints = cell(n, length(rawPoints));
sortedPointsCnt = 0;
unsortedPointsCnt = 0;
averageMeanY = [];
% Go through each frame in sequence
for frame = 1:length(rawPoints)
    % Init variables
    points = rawPoints{frame};
    points = sortrows(points,1);  % sort by x axis
    averageMeanY = [averageMeanY zeros(n, 1)]; % add new column to store
    maxROI = 60;
    resX = 2;  % window resolutions
    resY = 1.5;
    numWindows = round(maxROI/resX);  % number of sliding windows
    lines = cell(n, 1);
    l = 1;
    
    if plotWindows
        hold off
        scatter(points(:,2),points(:,1),10,'filled','square');
        xlim([-8,8])
        ylim([0,65])
        hold on
    end

    while true
        % Get starting point of the lane (first x point, since sorted by x)
        % break if no points left
        if isempty(points)
            lines{l} = NaN;
            averageMeanY(l, frame) = NaN;
            break
        else
            startX = points(1,1);
            startY = points(1,2);
        end

        % Initialize first window with resolution resX.resY (2D, omiting Z axis)
        window = [startX - resX/2 startX + resX/2 startY - resY/2 startY + resY/2];
        meanXY = []; % size changes, no mean to store if no points found
        line = []; % to store found points
        emptyWindow = 0;
        for w = 1:numWindows
            % If too many conseq. empty windows or outside ROI, there are probably no lane points left
            if emptyWindow > numWindows/2 || window(1) > 60
                break
            end

            insidePointsIdx = points(:,1) >= window(1) & points(:,1) <= window(2) & ...
                       points(:,2) >= window(3) & points(:,2) <= window(4);
            insidePoints = points(insidePointsIdx,:);

            % Plot sliding windows
            if plotWindows && plotLines
                y = [window(1), window(2), window(2), window(1), window(1)];
                x = [window(3), window(3), window(4), window(4), window(3)];
                plot(x, y, 'r-')
                hold on
            end

            % If points found, slide to the next window and shift by mean
            if ~isempty(insidePoints)
                meanXY = [meanXY; [mean(insidePoints(:,1)) mean(insidePoints(:,2))]];  % store mean if poitns found
                window(1:2) = window(1:2) + resX;
                window(3) = meanXY(end,2) - resY/2;
                window(4) = meanXY(end,2) + resY/2;
                line = [line; insidePoints];  % add points to lane
                points(insidePointsIdx == 1,:) = [];  % delete found points
            else  % If no points found, predict new window position
                emptyWindow = emptyWindow + 1;
                % Predict by shifting by last 2 means difference
                if size(meanXY,1) == 2
                    diff = meanXY(1,2) - meanXY(2,2);
                    window(1:2) = window(1:2) + resX;
                    window(3:4) = window(3:4) + diff;
                % Predict using 1D polynomial fitting
                elseif size(meanXY,1) > 2
                    polynomial = polyfit(meanXY(:,1), meanXY(:,2), 1);
                    window(1:2) = window(1:2) + resX;
                    y = polyval(polynomial, window(1) + resX/2);  % evaluate at next window centre
                    window(3) = y - resY/2;
                    window(4) = y + resY/2;
                else % Not enough points, just shift
                    window(1:2) = window(1:2) + resX;
                end
            end
        end

        % Save just found lane
        lines{l} = line;
        averageMeanY(l, end) = mean(meanXY(:,2));
        
        % Stop when all lanes found
        if l == n
            break
        end

        l = l + 1;
        
    end

    [sortedPoints{1:n, frame}] = deal(lines{:,1});
    unsortedPointsCnt = unsortedPointsCnt + numel(points(:,1));

    for s = 1:n
        sortedPointsCnt = sortedPointsCnt + numel(sortedPoints{s, frame})/3;
    end

    % Plot results
    if plotLines
        for x = 1:n
            txt = ['Lane',num2str(x)];
            scatter(sortedPoints{x,frame}(:,2),sortedPoints{x,frame}(:,1),10,'filled','square','DisplayName',txt)
            hold on
            xlim([-8,8])
            ylim([0,65])
            title(frame)
            legend
        end
        scatter(points(:,2),points(:,1),10,'filled','square','DisplayName','Unsorted')
        pause(0.2)
        hold off
    end

end

% Sort the lines in the cells so they belong to the same dimension
% Following lines essentially start with mean of the first cell and find
% the closest mean from the next column (frame) of cells and so on, marking
% what cells were already sorted.
sortedLines = cell(n, length(rawPoints));
for line = 1:n
    for frame = 1:length(rawPoints)
        if ~isnan(averageMeanY(line,1))
            average = averageMeanY(line,1);
            averageMeanY(line,1) = NaN;
            start = frame + 1;
            break
        end
    end

    sortedLines{line,1} = sortedPoints{line,1};
    idx = NaN;
    for frame = start:length(rawPoints)
        for i = 1:n
            if ~isnan(averageMeanY(i, frame))
                diff = abs(average - averageMeanY(i, frame));
                idx = i;
                break
            end
        end
        
        for i = idx:n
            if isnan(averageMeanY(i, frame))
                continue
            elseif abs(average - averageMeanY(i, frame)) < diff
                diff = abs(average - averageMeanY(i, frame));
                idx = i;
            end
        end

        sortedLines{line,frame} = sortedPoints{idx,frame};
        average = averageMeanY(idx,frame);
        averageMeanY(idx,frame) = NaN;
    end
end

unsortedPercentage = unsortedPointsCnt*100/(sortedPointsCnt + unsortedPointsCnt);
disp(['Undosrted points ', num2str(unsortedPercentage), ' %'])

save([seqName, '/lanesequence_', seqName, '_sorted.mat'], 'sortedLines')

end